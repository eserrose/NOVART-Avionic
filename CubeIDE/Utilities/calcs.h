/*
 * algebra.h
 *
 *  Created on: 17 Ağu 2021
 *      Author: EserRose
 */

#ifndef CALCS_H_
#define CALCS_H_

#include <math.h>
#include "MedianFilter/fast_median_filter.h"

#define P0		101325

static inline float norm(double x, double y, double z){
	return sqrt(x*x + y*y + z*z);
}

static inline int norm_int(int x, int y, int z){
	return sqrt(x*x + y*y + z*z);
}

static inline float calc_altitude(float pressure, float temperature){
	return -log(pressure/P0)*temperature/0.0341597;
}


#endif /* CALCS_H_ */
