/*
 * spi.c
 *
 *  Created on: Jul 25, 2021
 *      Author: E.Eser Gul
 */

#include "stm32f0xx_hal.h"
#include "main.h"
#include "spi.h"

#define SPI_MAX_TIMEOUT		5000

#define SLAVESELECT()  		HAL_GPIO_WritePin(GPIOB, FLASH_CS_Pin, GPIO_PIN_RESET)
#define SLAVEDESELECT()  	HAL_GPIO_WritePin(GPIOB, FLASH_CS_Pin, GPIO_PIN_SET)

SPI_HandleTypeDef hspi1;

void MX_SPI1_Init(void)
{

  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
}

int8_t SPI_Transfer(uint8_t *tx_data, uint16_t tx_len, uint8_t *rx_data, uint16_t rx_len)
{
	uint8_t status = 0;

	SLAVESELECT();

	if(tx_data)
		status = HAL_SPI_Transmit(&hspi1, tx_data, tx_len, SPI_MAX_TIMEOUT);

	if(rx_data)
		status = HAL_SPI_Receive(&hspi1, rx_data, rx_len, SPI_MAX_TIMEOUT);

	SLAVEDESELECT();

	return status == HAL_OK ? 1 : -1;
}
