/*
 * spi.h
 *
 *  Created on: Jul 25, 2021
 *      Author: E.Eser Gul
 */

#ifndef SPI_H_
#define SPI_H_

void MX_SPI1_Init(void);
int8_t SPI_Transfer(uint8_t *tx_data, uint16_t tx_len, uint8_t *rx_data, uint16_t rx_len);

#endif /* SPI_H_ */
