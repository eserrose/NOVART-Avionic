/*
 * i2c.c
 *
 *  Created on: Jul 24, 2021
 *      Author: E.Eser Gul
 */

#include "stm32f0xx_hal.h"
#include "i2c.h"
#include "main.h"

I2C_HandleTypeDef hi2c1;

void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analog filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }

}

int8_t i2c1_write(uint16_t addr, uint8_t *buf, uint16_t mem_addr, uint16_t tx_len)
{
	if (HAL_I2C_Mem_Write(&hi2c1, addr, mem_addr, tx_len, buf, tx_len, I2C_MAX_DELAY) == HAL_OK)
		return 1;
	else
		return -1;
}

int8_t i2c1_read(uint16_t addr, uint8_t *buf, uint16_t mem_addr, uint16_t rx_len)
{
	if (HAL_I2C_Mem_Read(&hi2c1, addr, mem_addr, 1, buf, rx_len, I2C_MAX_DELAY) == HAL_OK)
		return 1;
	else
		return -1;
}
