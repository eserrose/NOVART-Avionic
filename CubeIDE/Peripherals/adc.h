/*
 * adc.h
 *
 *  Created on: Jul 26, 2021
 *      Author: E.Eser Gul
 */

#ifndef ADC_H_
#define ADC_H_

#define ADC_NO_CHANNELS		4
#define VBAT_CONV_FACTOR	0.00449797f

void MX_ADC_Init(void);
int8_t ADC_Read(uint32_t *pData);

#endif /* ADC_H_ */
