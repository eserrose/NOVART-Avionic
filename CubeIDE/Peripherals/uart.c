/*
 * uart.c
 *
 *  Created on: Jul 26, 2021
 *      Author: E.Eser Gul
 */


#include <string.h>

#include "printf.h"
#include "stm32f0xx_hal.h"
#include "uart.h"

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

static void MX_USART1_UART_Init(void)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 38400;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
}

static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

void uart_init(void){
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
}

int8_t uart1_transmit(uint8_t *txData, uint16_t len){
	if (HAL_UART_Transmit(&huart1, txData, len, UART_MAX_TIMEOUT) == HAL_OK)
		return 1;

	return -1;
}

int8_t uart1_receive(uint8_t *rxData, uint16_t len){
	if (HAL_UART_Receive(&huart1, rxData, len, UART_MAX_TIMEOUT) == HAL_OK){
		return 1;
	}
	return -1;
}

int8_t uart2_transmit(uint8_t *txData, uint16_t len){
	if (HAL_UART_Transmit(&huart2, txData, len, UART_MAX_TIMEOUT) == HAL_OK)
		return 1;

	return -1;
}

int8_t uart_ext_print(const uint8_t *text)
{
	return uart2_transmit((uint8_t*) text, strlen((char*)text));
}

int8_t uart_ext_printf(const char *format, ...)
{
	va_list ap;
	uint8_t buffer[UART_EXT_MAX_LEN];
	va_start(ap, format);
	vsnprintf((char*)buffer, UART_EXT_MAX_LEN, format, ap);
	va_end(ap);
	return uart2_transmit(buffer, strlen((char*)buffer));
}
