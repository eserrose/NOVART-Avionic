/*
 * uart.h
 *
 *  Created on: Jul 26, 2021
 *      Author: E.Eser Gul
 */

#ifndef UART_H_
#define UART_H_

#include "main.h"

#include "FreeRTOS.h"
#include "semphr.h"

#define UART_EXT_MAX_LEN		 30
#define UART_MAX_TIMEOUT	   2000

#define PRINT(...)				uart_ext_print((const uint8_t*) x)
#define PRINTF(...)				uart_ext_printf(__VA_ARGS__)

#ifdef USE_UART_DEBUG
#define DEBUG_PRINT(x)			uart_ext_print((const uint8_t*) x)
#define DEBUG_PRINTF(...)		uart_ext_printf(__VA_ARGS__)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTF(...)
#endif

void uart_init(void);
int8_t uart1_transmit(uint8_t *txData, uint16_t len);
int8_t uart2_transmit(uint8_t *txData, uint16_t len);
int8_t uart1_receive(uint8_t *rxData, uint16_t len);
int8_t uart_ext_print(const uint8_t *text);
int8_t uart_ext_printf(const char *format, ...);

#endif /* UART_H_ */
