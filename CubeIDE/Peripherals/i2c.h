/*
 * i2c.h
 *
 *  Created on: Jul 24, 2021
 *      Author: E.Eser Gul
 */

#ifndef I2C_H_
#define I2C_H_

#define I2C_MAX_DELAY 	200

void MX_I2C1_Init(void);

int8_t i2c1_write(uint16_t addr, uint8_t *buf, uint16_t mem_addr, uint16_t tx_len);
int8_t i2c1_read(uint16_t addr, uint8_t *buf, uint16_t mem_addr, uint16_t rx_len);

extern I2C_HandleTypeDef hi2c1;

#endif /* I2C_H_ */
