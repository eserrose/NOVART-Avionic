/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

#include "FreeRTOS.h"
#include "task.h"
#include "uart.h"
#include "i2c.h"
#include "spi.h"
#include "adc.h"

#include "Sensors.h"
#include "EGU_driver.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/

#define MAIN_TASK_PERIOD_MS	    20U
#define MAIN_TASK_PRIORITY		  4
#define MAIN_TASK_STACK			400

#define COMM_TASK_PERIOD_MS	   600U
#define COMM_TASK_PRIORITY		  4
#define COMM_TASK_STACK			500

#define MIN_TIME			10*1000U	//10 seconds

/* Private macro -------------------------------------------------------------*/
#define LED1_ON()		HAL_GPIO_WritePin(LED_1_GPIO_Port,LED_1_Pin,GPIO_PIN_SET)
#define LED2_ON()		HAL_GPIO_WritePin(LED_2_GPIO_Port,LED_2_Pin,GPIO_PIN_SET)
#define LED1_OFF()		HAL_GPIO_WritePin(LED_1_GPIO_Port,LED_1_Pin,GPIO_PIN_RESET)
#define LED2_OFF()		HAL_GPIO_WritePin(LED_2_GPIO_Port,LED_2_Pin,GPIO_PIN_RESET)
#define BUZZER_ON()		HAL_GPIO_WritePin(BUZZER_TRIGGER_GPIO_Port,BUZZER_TRIGGER_Pin,GPIO_PIN_SET)
#define BUZZER_OFF()	HAL_GPIO_WritePin(BUZZER_TRIGGER_GPIO_Port,BUZZER_TRIGGER_Pin,GPIO_PIN_RESET)


/* Private variables ---------------------------------------------------------*/
long int t0;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
static void prvMainTask( void *pvParameters );
static void prvCommTask( void *pvParameters );

static void init_before_scheduler(void)
{
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_ADC_Init();
	MX_I2C1_Init();
	MX_SPI1_Init();
	uart_init();
	init_sensors();
}

static void init_after_scheduler(void)
{
	uint8_t status;

	BUZZER_ON();
	HAL_Delay(500);
	BUZZER_OFF();
	HAL_Delay(500);
	if(EGU_Get_Status(&status) > 0){
		BUZZER_ON();
		HAL_Delay(1000);
		BUZZER_OFF();
		DEBUG_PRINTF("EGU Status: %x\n", status);
		uint16_t time = 0xffff;
		if(EGU_Buzz((uint8_t*)&time) > 0)
			DEBUG_PRINT("Buzz success\n");
	}

	xTaskCreate(prvMainTask,"maintask", MAIN_TASK_STACK,NULL,MAIN_TASK_PRIORITY,NULL);
	xTaskCreate(prvCommTask,"commtask", COMM_TASK_STACK,NULL,COMM_TASK_PRIORITY,NULL);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

	init_before_scheduler();

	DEBUG_PRINT("Starting scheduler...\n");
	vTaskStartScheduler();

	for( ;; );

	return 0;

  //todo: Add algorithms and kalman filter
  //todo: batarya gerilimi
}

static void prvMainTask( void *pvParameters ){

	uint8_t state = READ_REGISTRY(LAST_SEQ);

	if(state != ACCL && state != DESC){
		state = RAMP;
		DEBUG_PRINT("Waiting on ramp\n");
		WRITE_REGISTRY(LAST_SEQ, state);
		record_altitude();
	}

	for(;;){

		switch(state){
			case RAMP:
				if(is_accelerating()){
					t0 = HAL_GetTick();
					state = ACCL;
					DEBUG_PRINT("Flight started\n.");
				}
				break;
			case ACCL:
				if(is_descending() && (t0 - HAL_GetTick()) > MIN_TIME){
					state = DESC;
					DEBUG_PRINT("Apogee reached\n.");
				}
				break;
			case DESC:
				fire();
				state++;
				break;
			default:
				break;
			}

		Use_Sensors();
		vTaskDelay(MAIN_TASK_PERIOD_MS/portTICK_RATE_MS);
	}
}

static void prvCommTask( void *pvParameters ){

	uint8_t buf[EGU_RSP_LEN_DATA_CONV];

	for(;;){
		LED2_ON();
		Send_Sensor_Data();
		if(EGU_Get_TM(buf, 0x00) > 0)
			DEBUG_PRINT(buf);
		LED2_OFF();
		vTaskDelay(COMM_TASK_PERIOD_MS/portTICK_RATE_MS);
	}
}

void vApplicationDaemonTaskStartupHook( void )
{
	init_after_scheduler();
}

void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
}

void vApplicationMallocFailedHook(void)
{
	taskDISABLE_INTERRUPTS();
	DEBUG_PRINT("malloc failed, restarting\n");
	NVIC_SystemReset();
}

void vApplicationIdleHook( void )
{
}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}


/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(FLASH_RESET_GPIO_Port, FLASH_RESET_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, FLASH_CS_Pin|FLASH_WP_Pin|LED_1_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, HIGH_2_LS_TRIGGER_Pin|LOW_1_LS_TRIGGER_Pin|LOW_2_LS_TRIGGER_Pin|LED_2_Pin
                          |HIGH_HS_TRIGGER_Pin|BUZZER_TRIGGER_Pin|HIGH_1_LS_TRIGGER_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LOW_HS_TRIGGER_GPIO_Port, LOW_HS_TRIGGER_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : FLASH_RESET_Pin LOW_HS_TRIGGER_Pin */
  GPIO_InitStruct.Pin = FLASH_RESET_Pin|LOW_HS_TRIGGER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : FLASH_CS_Pin */
  GPIO_InitStruct.Pin = FLASH_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
  HAL_GPIO_Init(FLASH_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : FLASH_WP_Pin HIGH_2_LS_TRIGGER_Pin LOW_1_LS_TRIGGER_Pin LOW_2_LS_TRIGGER_Pin
                           LED_1_Pin LED_2_Pin HIGH_HS_TRIGGER_Pin BUZZER_TRIGGER_Pin
                           HIGH_1_LS_TRIGGER_Pin */
  GPIO_InitStruct.Pin = FLASH_WP_Pin|HIGH_2_LS_TRIGGER_Pin|LOW_1_LS_TRIGGER_Pin|LOW_2_LS_TRIGGER_Pin
                          |LED_1_Pin|LED_2_Pin|HIGH_HS_TRIGGER_Pin|BUZZER_TRIGGER_Pin
                          |HIGH_1_LS_TRIGGER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
