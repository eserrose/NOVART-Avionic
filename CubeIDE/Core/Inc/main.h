/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define VBAT_ADC_Pin GPIO_PIN_0
#define VBAT_ADC_GPIO_Port GPIOA
#define FLASH_RESET_Pin GPIO_PIN_4
#define FLASH_RESET_GPIO_Port GPIOA
#define FLASH_CS_Pin GPIO_PIN_0
#define FLASH_CS_GPIO_Port GPIOB
#define FLASH_WP_Pin GPIO_PIN_1
#define FLASH_WP_GPIO_Port GPIOB
#define HIGH_2_LS_TRIGGER_Pin GPIO_PIN_2
#define HIGH_2_LS_TRIGGER_GPIO_Port GPIOB
#define LOW_1_LS_TRIGGER_Pin GPIO_PIN_10
#define LOW_1_LS_TRIGGER_GPIO_Port GPIOB
#define LOW_2_LS_TRIGGER_Pin GPIO_PIN_12
#define LOW_2_LS_TRIGGER_GPIO_Port GPIOB
#define LED_1_Pin GPIO_PIN_13
#define LED_1_GPIO_Port GPIOB
#define LED_2_Pin GPIO_PIN_14
#define LED_2_GPIO_Port GPIOB
#define LOW_HS_TRIGGER_Pin GPIO_PIN_12
#define LOW_HS_TRIGGER_GPIO_Port GPIOA
#define HIGH_HS_TRIGGER_Pin GPIO_PIN_5
#define HIGH_HS_TRIGGER_GPIO_Port GPIOB
#define BUZZER_TRIGGER_Pin GPIO_PIN_8
#define BUZZER_TRIGGER_GPIO_Port GPIOB
#define HIGH_1_LS_TRIGGER_Pin GPIO_PIN_9
#define HIGH_1_LS_TRIGGER_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define USE_UART_DEBUG
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
