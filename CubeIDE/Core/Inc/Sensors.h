/*
 * Sensors.h
 *
 *  Created on: Aug 17, 2021
 *      Author: Eser Gul
 */

#ifndef INC_SENSORS_H_
#define INC_SENSORS_H_

#include <calcs.h>
#include <string.h>

#include "bmp280.h"
#include "mpu6050.h"
#include "QMC5883.h"
#include "MX25L.h"

#define CHECK_NO	 4
#define FILTER_NO	 3
#define MIN_DELAY	20U
#define MIN_ACC		 4

typedef struct EGU_Sensors {
	BMP280_t  bmp;
	QMC_t	  qmc;
	MPU6050_t mpu;
}Sensor_t;

Sensor_t Sensors;
tMedianFilter mfilter;
tMedianFilter afilter;

float mfilter_arr[FILTER_NO];
float afilter_arr[FILTER_NO];
size_t mfilter_size[FILTER_NO];
size_t afilter_size[FILTER_NO];

static void init_sensors(void){
	uint8_t data[3];

	bmp280_init_default_params(&(Sensors.bmp).params);

	if(!bmp280_init(&(Sensors.bmp), &(Sensors.bmp).params, &hi2c1)) {
		Error_Handler();
	}

	if(!MPU6050_Init(&(Sensors.mpu), &hi2c1)) {
		Error_Handler();
	}

	if(!QMC_init(&(Sensors.qmc), &hi2c1, QMC_DATA_RATE_200)) {
		Error_Handler();
	}

	if(MX25L_Get_ID(data) < 1 || data[0] != MANUFACTURER_ID)
		Error_Handler();

	mfilter.data_array = mfilter_arr;
	mfilter.index_array = mfilter_size;
	afilter.data_array = afilter_arr;
	afilter.index_array = afilter_size;
}

static void Use_Sensors(void){
	bmp280_read_float(&(Sensors.bmp));
	MPU6050_Read_All(&(Sensors.mpu));
	QMC_read(&(Sensors.qmc));
}

static float get_voltage(void){
	uint32_t raw;
	if(ADC_Read(&raw) < 1)
		return -1;

	return raw*VBAT_CONV_FACTOR;
}

static void Send_Sensor_Data(void){
	float vol = get_voltage();
	float alt = calc_altitude(Sensors.bmp.pressure, Sensors.bmp.temperature) - (int32_t)READ_REGISTRY(ALTITUDE);

	DEBUG_PRINTF("%5.2f,%3.2f,%3.2f,%3.2f\n", alt, Sensors.mpu.Ax, Sensors.mpu.Ay, Sensors.mpu.Az);
	vTaskDelay(500/portTICK_RATE_MS);
	DEBUG_PRINTF("%3.2f,%3.2f,%3.2f\n",Sensors.mpu.Gx, Sensors.mpu.Gy, Sensors.mpu.Gz);
	vTaskDelay(500/portTICK_RATE_MS);
	DEBUG_PRINTF("%2.4f,%2.4f,%2.4f\n",Sensors.qmc.Xaxis, Sensors.qmc.Yaxis, Sensors.qmc.Zaxis);
	vTaskDelay(500/portTICK_RATE_MS);
	DEBUG_PRINTF("Roll: %.3f, Pitch: %.3f\n", Sensors.mpu.KalmanAngleX, Sensors.mpu.KalmanAngleY);
	DEBUG_PRINTF("VBAT %.2f\n", vol);
}

static void	record_altitude(void){
	float alt = -1;

	if(bmp280_read_float(&(Sensors.bmp)))
		alt = calc_altitude(Sensors.bmp.pressure, Sensors.bmp.temperature);

	DEBUG_PRINTF("Altitude %.2f recorded\n", alt);

	if(alt != -1){
		WRITE_REGISTRY(ALTITUDE, (int32_t)alt);
	}

}

static void fire(void)
{
	DEBUG_PRINT("Performing ignition\n.");
	HAL_GPIO_WritePin(GPIOB,HIGH_HS_TRIGGER_Pin,GPIO_PIN_SET);
	vTaskDelay(100/portTICK_RATE_MS);
	HAL_GPIO_WritePin(GPIOB,HIGH_1_LS_TRIGGER_Pin|HIGH_2_LS_TRIGGER_Pin,GPIO_PIN_SET);
	vTaskDelay(1000/portTICK_RATE_MS);
	HAL_GPIO_WritePin(GPIOB,HIGH_HS_TRIGGER_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB,HIGH_1_LS_TRIGGER_Pin|HIGH_2_LS_TRIGGER_Pin,GPIO_PIN_RESET);
}


static uint8_t is_accelerating(){
	for(uint8_t i = 0; i < FILTER_NO; i++){
		FastMedianFilter(&afilter, norm(Sensors.mpu.Az,Sensors.mpu.Ay,Sensors.mpu.Az));
		vTaskDelay(MIN_DELAY/portTICK_RATE_MS);
	}

	return FastMedianFilter(&afilter, norm(Sensors.mpu.Az,Sensors.mpu.Ay,Sensors.mpu.Az)) > MIN_ACC;
}

static float get_altitude(){

	for(uint8_t i = 0; i < FILTER_NO; i++){
		FastMedianFilter(&mfilter, calc_altitude(Sensors.bmp.pressure, Sensors.bmp.temperature));
		vTaskDelay(MIN_DELAY/portTICK_RATE_MS);
	}

	return FastMedianFilter(&mfilter, calc_altitude(Sensors.bmp.pressure, Sensors.bmp.temperature)) - (int32_t)READ_REGISTRY(ALTITUDE);
}

static uint8_t is_descending(){

	float alts[2] = {0};

	for(uint8_t i = 0; i < 2; i++){
		for(uint8_t j = 0; j < CHECK_NO; j++){
			alts[i] += get_altitude();
			vTaskDelay(MIN_DELAY/portTICK_RATE_MS);
		}
		alts[i]/=CHECK_NO;
	}

	return alts[0] > alts[1];
}

#endif /* INC_SENSORS_H_ */
