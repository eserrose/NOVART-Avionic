/*
 * QMC5883.c
 *
 *  Created on: 11 May 2021
 *      Author: Serdar
 */
#include"QMC5883.h"
#include "math.h"

//###############################################################################################################
int8_t QMC_init(QMC_t *qmc,I2C_HandleTypeDef *i2c,uint8_t Output_Data_Rate)
{
	uint8_t buf = 1;

	qmc->i2c = i2c;
	qmc->Control_Register = (QMC_CONT | QMC_OSR_512 | QMC_FR_2 | Output_Data_Rate);

	if(HAL_I2C_Mem_Write(qmc->i2c, 0x1A, 0x0B, 1, &buf, 1, 100)!=HAL_OK) return -1;
	if(HAL_I2C_Mem_Write(qmc->i2c, 0x1A, 0x09, 1, &(qmc->Control_Register), 1, 100) != HAL_OK) return -1;

	return -1;
}

int8_t QMC_read(QMC_t *qmc)
{
	  uint8_t buf;
	  uint16_t sens = 0;

	  if(HAL_I2C_Mem_Read(qmc->i2c, 0x1A, 0x09, 1, &buf, 1, 100) != HAL_OK)
		  return -1;

	  if( (buf & QMC_RNG_MASK) == QMC_FR_2) sens = 12000;
	  else if( (buf & QMC_RNG_MASK) == QMC_FR_8) sens = 3000;

	  qmc->datas[0]=0;
	  HAL_I2C_Mem_Read(qmc->i2c, 0x1A, 0x06, 1, qmc->datas, 1, 100);

	  if((qmc->datas[0]&0x01)==1)
	  {
		  HAL_I2C_Mem_Read(qmc->i2c, 0x1A, 0x00, 1, qmc->datas, 6, 100);
		  qmc->Xaxis= (float)((int16_t)((qmc->datas[1]<<8) | qmc->datas[0]))/sens;
		  qmc->Yaxis= (float)((int16_t)((qmc->datas[3]<<8) | qmc->datas[2]))/sens;
		  qmc->Zaxis= (float)((int16_t)((qmc->datas[5]<<8) | qmc->datas[4]))/sens;

		  qmc->compas=atan2f(qmc->Yaxis,qmc->Xaxis)*180.00/M_PI;

		  if(qmc->compas>0)
		  {
			  qmc->heading= qmc->compas;
		  }
		  else
		  {
			  qmc->heading=360+qmc->compas;
		  }

		  return 1;
	  }

	  return -1;
}

float QMC_readHeading(QMC_t *qmc)
{
	QMC_read(qmc);
	return qmc->heading;
}

int8_t QMC_Standby(QMC_t *qmc)
{
	uint8_t array[1]={0};
	if(HAL_I2C_Mem_Write(qmc->i2c, 0x1A, 0x09, 1, array, 1, 100)!=HAL_OK) return -1;
	return 1;
}
int8_t QMC_Reset(QMC_t *qmc)
{
	uint8_t array[1]={0x80};
	if(HAL_I2C_Mem_Write(qmc->i2c, 0x1A, 0x0A, 1, array, 1, 100)!=HAL_OK) return -1;
	return 1;
}
