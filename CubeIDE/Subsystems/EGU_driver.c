/*
 * EGU_driver.c
 *
 *  Created on: Aug 17, 2021
 *      Author: Eser Gul
 */

#include "uart.h"
#include "EGU_driver.h"

int8_t EGU_send_command(uint8_t id, uint8_t *txdata, uint8_t *rxdata, uint16_t len){
	int8_t status;
	uint8_t buf[EGU_TC_LEN] = {id, txdata[0], txdata[1], get_checksum(id,txdata)};

	status = uart1_transmit(buf, EGU_TC_LEN);

	if(status > 0)
		status = uart1_receive(rxdata, len);

	return status;
}

int8_t EGU_Get_Status(uint8_t *data){
	uint8_t buf[2] = {0,0};
	return EGU_send_command(EGU_TC_ID_GET_STATUS, buf, data, EGU_RSP_LEN_STATUS);
}

int8_t EGU_Get_TM(uint8_t *data, uint8_t isRaw){
	uint8_t buf[2] = {isRaw,0};
	return EGU_send_command(EGU_TC_ID_GET_DATA, buf, data, EGU_RSP_LEN_DATA_RAW);
}

int8_t EGU_Fire();
int8_t EGU_Ping();

int8_t EGU_Buzz(uint8_t *time){
	uint8_t data;
	uint8_t buf[2] = {time[0],time[1]};

	if(EGU_send_command(EGU_TC_ID_BUZZER_ON, buf, &data, EGU_RSP_LEN_BUZZER) > 0)
		return data == 0x01;

	return -1;
}

uint8_t get_checksum(uint8_t id, uint8_t *data){
	return id + data[0] + data[1];
}
