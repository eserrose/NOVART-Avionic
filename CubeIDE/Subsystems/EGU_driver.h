/*
 * EGU_driver.h
 *
 *  Created on: Aug 17, 2021
 *      Author: Eser Gul
 */

#ifndef EGU_DRIVER_H_
#define EGU_DRIVER_H_

#define EGU_TC_ID_BUZZER_ON		0x01
#define EGU_TC_ID_BUZZER_OFF	0x02
#define EGU_TC_ID_GET_DATA		0x03
#define EGU_TC_ID_GET_LOG		0x04
#define EGU_TC_ID_GET_STATUS	0x05
#define EGU_TC_ID_FIRE			0x07
#define EGU_TC_ID_PING			0xFF

#define EGU_TC_LEN				4U

#define EGU_RSP_LEN_BUZZER		1U
#define EGU_RSP_LEN_DATA_RAW	32U
#define EGU_RSP_LEN_DATA_CONV   91U
#define EGU_RSP_LEN_LOG			256U
#define EGU_RSP_LEN_STATUS		2U
#define EGU_RSP_LEN_PING		1U

#define EGU_TM_PRESSURE_CONV	(1/256)
#define EGU_TM_TEMP_CONV		(1/100)
#define EGU_TM_ACC_CONV			(1/16384)
#define EGU_TM_GYRO_CONV		(1/131)
#define EGU_TM_MTM_CONV			(1/12000)

#define EGU_STATUS_SEPERATED	0x80
#define EGU_STATUS_SEQUENCE		0x01
#define EGU_STATUS_ERRORS		0x7E

int8_t EGU_send_command(uint8_t id, uint8_t *data, uint8_t *rxdata, uint16_t len);

int8_t EGU_Get_Status(uint8_t *data);
int8_t EGU_Get_TM(uint8_t *data, uint8_t isRaw);
int8_t EGU_Fire();
int8_t EGU_Ping();
int8_t EGU_Buzz(uint8_t *time);

uint8_t get_checksum(uint8_t id, uint8_t *data);

#endif /* EGU_DRIVER_H_ */
