/*
 * mpu6050.h
 *
 *  Created on: Nov 13, 2019
 *      Author: Bulanov Konstantin
 */

#ifndef INC_GY521_H_
#define INC_GY521_H_

#endif /* INC_GY521_H_ */

#include <stdint.h>

// MPU6050 structure
typedef struct
{

	I2C_HandleTypeDef *i2c_handle;

    int16_t Accel_X_RAW;
    int16_t Accel_Y_RAW;
    int16_t Accel_Z_RAW;
    double Ax;
    double Ay;
    double Az;

    int16_t Gyro_X_RAW;
    int16_t Gyro_Y_RAW;
    int16_t Gyro_Z_RAW;
    double Gx;
    double Gy;
    double Gz;

    float Temperature;

    double KalmanAngleX;
    double KalmanAngleY;
} MPU6050_t;

// Kalman structure
typedef struct
{
    double Q_angle;
    double Q_bias;
    double R_measure;
    double angle;
    double bias;
    double P[2][2];
} Kalman_t;

int8_t MPU6050_Init(MPU6050_t *DataStruct, I2C_HandleTypeDef *I2Cx);

int8_t MPU6050_Read_Accel(MPU6050_t *DataStruct);

int8_t MPU6050_Read_Gyro(MPU6050_t *DataStruct);

int8_t MPU6050_Read_Temp(MPU6050_t *DataStruct);

int8_t MPU6050_Read_All(MPU6050_t *DataStruct);

double Kalman_getAngle(Kalman_t *Kalman, double newAngle, double newRate, double dt);
