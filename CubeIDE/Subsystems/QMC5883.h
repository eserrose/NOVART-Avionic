/*
 * QMC5883.h
 *
 *  Created on: 11 May 2021
 *      Author: Serdar
 */

#ifndef QMC5883_H_
#define QMC5883_H_
//#########################################################################################################
#ifdef __cplusplus
 extern "C" {
#endif
//#########################################################################################################
#include "main.h"
 //#########################################################################################################
#define QMC_STANDBY 0
#define QMC_CONT 	1
#define QMC_OK 		0
#define QMC_FALSE 	1
//#########################################################################################################

#define QMC_DATA_RATE_200		0x0C
#define QMC_DATA_RATE_100		0x08
#define QMC_DATA_RATE_50		0x04
#define QMC_DATA_RATE_10		0x00

#define QMC_OSR_512				0x00

#define QMC_RNG_MASK			0x30
#define QMC_FR_2				0x00
#define QMC_FR_8				0x10

typedef struct QMC
{
	I2C_HandleTypeDef   *i2c;
	uint8_t				Control_Register;
	uint8_t             datas[6];
	float             	Xaxis;
	float             	Yaxis;
	float             	Zaxis;
	float			    heading;
	float               compas;
}QMC_t;
//#########################################################################################################
int8_t QMC_init(QMC_t *qmc,I2C_HandleTypeDef *i2c,uint8_t Output_Data_Rate);
int8_t QMC_read(QMC_t *qmc);
float  QMC_readHeading(QMC_t *qmc);
int8_t QMC_Standby(QMC_t *qmc);
int8_t QMC_Reset(QMC_t *qmc);

//#########################################################################################################
#ifdef __cplusplus
}
#endif
#endif /* QMC5883_H_ */
