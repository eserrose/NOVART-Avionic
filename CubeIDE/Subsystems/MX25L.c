/*
* The MIT License (MIT)
*
* Copyright (c) 2021 E.Eser Gul <eserrose@gmail.com>
*
* MX25L.c
* This file is part of the MX25L SPI memory library.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
#include <stdint.h>
#include <string.h>
#include "MX25L.h"
#include "spi.h"

#define ENABLE_WRITE()		MX25L_Set_Write_Enable(1)

int8_t MX25L_Get_ID(uint8_t data[3])
{
	uint8_t cmd[1] = {COMMAND_RDID};
	return SPI_Transfer(cmd, COMMAND_RDID_LEN, data, RSP_RDID_LEN);
}

int8_t MX25L_Get_Status(uint8_t *status)
{
	uint8_t cmd[1] = {COMMAND_RDSR};
	return SPI_Transfer(cmd, COMMAND_RDSR_LEN, status, RSP_RDSR_LEN);
}

int8_t MX25L_Set_Write_Enable(bool enable)
{
	uint8_t cmd[1] = {enable ? COMMAND_WREN : COMMAND_WRDI};
	return SPI_Transfer(cmd, COMMAND_WREN_LEN, NULL, RSP_WREN_LEN);
}

int8_t MX25L_Set_Status(uint8_t stat)
{
	int8_t status;
	uint8_t tmp;
	uint8_t cmd[COMMAND_WRSR_LEN - 1] = {COMMAND_WRSR, stat};

	MX25L_Set_Write_Enable(1);

	status = SPI_Transfer(cmd, COMMAND_WRSR_LEN - 1, NULL, 0);

	if(status > 0) {
		do{
			 status = MX25L_Get_Status(&tmp);
		} while( (tmp & STATUS_WIP) && status > 0);
	}

	MX25L_Set_Write_Enable(0);
	return status;
}

int8_t MX25L_Erase(uint32_t address, uint32_t length)
{
	int8_t status = 0;

	switch(length){
	case 0x1000:
		status = MX25L_Sector_Erase(address);
		break;
	case 0x8000:
		status = MX25L_Block_Erase(address, 0);
		break;
	case 0x10000:
		status = MX25L_Block_Erase(address, 1);
		break;
	}

	return status;
}


int8_t MX25L_Sector_Erase(uint32_t address)
{
	int8_t status;
	uint8_t tmp;
	uint8_t cmd[COMMAND_SE_LEN] = {COMMAND_SE, (uint8_t) (address >> 16), (uint8_t) (address >> 8), (uint8_t) address};

	MX25L_Set_Write_Enable(1);

	status = SPI_Transfer(cmd, COMMAND_SE_LEN, NULL, RSP_SE_LEN);

	if(status > 0) {
		do{
			 status = MX25L_Get_Status(&tmp);
		} while( (tmp & STATUS_WIP) && status > 0);
	}

	MX25L_Set_Write_Enable(0);
	return status;
}

int8_t MX25L_Block_Erase(uint32_t address, uint8_t is64)
{
	int8_t status;
	uint8_t tmp;
	uint8_t cmd[COMMAND_BE_LEN] = {is64 ? COMMAND_BE : COMMAND_BE_32K,
			(uint8_t) (address >> 16), (uint8_t) (address >> 8), (uint8_t) address};

	MX25L_Set_Write_Enable(1);

	status = SPI_Transfer(cmd, COMMAND_BE_LEN, NULL, RSP_BE_LEN);

	if(status > 0) {
		do{
			 status = MX25L_Get_Status(&tmp);
		} while( (tmp & STATUS_WIP) && status > 0);
	}

	MX25L_Set_Write_Enable(0);
	return status;
}

int8_t MX25L_Chip_Erase(void)
{
	int8_t status;
	uint8_t tmp;
	uint8_t cmd[1] = {COMMAND_CE};

	MX25L_Set_Write_Enable(1);

	status = SPI_Transfer(cmd, COMMAND_CE_LEN, NULL, RSP_CE_LEN);

	if(status > 0) {
		do{
			 status = MX25L_Get_Status(&tmp);
		} while( (tmp & STATUS_WIP) && status > 0);
	}

	MX25L_Set_Write_Enable(0);
	return status;
}

int8_t MX25L_Write(uint32_t address, const uint8_t * data, uint32_t length)
{
	int8_t status;
	uint8_t tmp;
	uint8_t cmd[COMMAND_WRITE_LEN + MAX_WRITE_LEN] = {COMMAND_WRITE,
			(uint8_t) (address >> 16), (uint8_t) (address >> 8), (uint8_t) address};

	memcpy(cmd + COMMAND_WRITE_LEN, data, length);

	MX25L_Set_Write_Enable(1);

	status = SPI_Transfer(cmd, COMMAND_WRITE_LEN + length, NULL, RSP_WRITE_LEN);

	if(status > 0) {
		do{
			 status = MX25L_Get_Status(&tmp);
		} while( (tmp & STATUS_WIP) && status > 0);
	}

	MX25L_Set_Write_Enable(0);
	return status;
}

int8_t MX25L_Read(uint32_t address, uint8_t * data, uint32_t length)
{
	int8_t status;
	uint8_t tmp;
	uint8_t cmd[COMMAND_READ_LEN] = {COMMAND_READ, (uint8_t) (address >> 16), (uint8_t) (address >> 8), (uint8_t) address};

	status = SPI_Transfer(cmd, COMMAND_READ_LEN, data, length);

	if(status > 0) {
		do{
			 status = MX25L_Get_Status(&tmp);
		} while( (tmp & STATUS_WIP) && status > 0);
	}

	return status;
}


int8_t MX25L_Protect_Block(bool bp0, bool bp1, bool bp2, bool bp3)
{
	uint8_t status;

	if(MX25L_Get_Status(&status) < 1)
		return -1;

	if(bp0) status |= STATUS_BP0;
	if(bp1) status |= STATUS_BP1;
	if(bp2) status |= STATUS_BP2;
	if(bp3) status |= STATUS_BP3;

	return MX25L_Set_Status(status);
}

int8_t MX25L_Check_Sectors(uint8_t *list)
{
	uint16_t page = 0, ctr = 0;
	uint8_t buffer[256] = {0}, ch = 0x21;

	while(page < NO_SECTORS){
		memset(buffer,ch,256);
		MX25L_Write(page*256, buffer, 256);

		memset(buffer,0,256);
		MX25L_Read(page*256, buffer, 256);

		for(uint16_t i = 0; i < 256; i++){
			if(buffer[i] != ch)
				list[ctr++] = page*256 + i;
		}
		page++;
		ch++;
	}

	return ctr == 0;
}

uint32_t READ_REGISTRY(uint32_t key){
	uint32_t buf;
	MX25L_Read(4*key,(uint8_t*) &buf, sizeof(buf));
	return buf;
}

int8_t WRITE_REGISTRY(uint32_t key, uint32_t data){
	uint8_t buf[4*REGISTRY_SIZE];

	if(MX25L_Read(LAST_SEQ, buf, 4*REGISTRY_SIZE) < 1)
		return -1;

	MX25L_Sector_Erase(LAST_SEQ);

	for(uint16_t i = 0; i < 4*REGISTRY_SIZE; i+=4){
		if(i != key*4)
			MX25L_Write(i, buf+i, 4);
		else if(MX25L_Write(i, (uint8_t*)&data, 4) < 1)
			return -1;
	}

	return 1;
}

