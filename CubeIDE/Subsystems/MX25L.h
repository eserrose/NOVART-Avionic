/*
* The MIT License (MIT)
*
* Copyright (c) 2021 E.Eser Gul <eserrose@gmail.com>
*
* MX25L.h
* This file is part of the MX25L SPI memory library.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#ifndef MX25L_H_
#define MX25L_H_

#define COMMAND_WREN   0x06
#define COMMAND_WRDI   0x04
#define COMMAND_RDSR   0x05
#define COMMAND_WRSR   0x01
#define COMMAND_READ   0x03
#define COMMAND_FREAD  0x0B
#define COMMAND_WRITE  0x02
#define COMMAND_RDID   0x9F
#define COMMAND_SE     0x20
#define COMMAND_BE_32K 0x52
#define COMMAND_BE     0xD8
#define COMMAND_CE     0x60

#define COMMAND_WREN_LEN	1U
#define COMMAND_WRDI_LEN	1U
#define COMMAND_RDSR_LEN	1U
#define COMMAND_WRSR_LEN	3U
#define COMMAND_READ_LEN	4U
#define COMMAND_FREAD_LEN	5U
#define COMMAND_WRITE_LEN	4U
#define COMMAND_RDID_LEN	1U
#define COMMAND_SE_LEN		4U
#define COMMAND_BE_LEN		4U
#define COMMAND_CE_LEN		1U

#define RSP_RDSR_LEN		1U
#define RSP_RDID_LEN		3U
#define RSP_WREN_LEN		0U
#define RSP_CE_LEN			0U
#define RSP_SE_LEN			0U
#define RSP_WRITE_LEN		0U
#define RSP_BE_LEN			0U

#define STATUS_WIP    0b00000001
#define STATUS_WEL    0b00000010
#define STATUS_BP0    0b00000100
#define STATUS_BP1    0b00001000
#define STATUS_BP2    0b00010000
#define STATUS_BP3    0b00100000
#define STATUS_RES    0b01000000
#define STATUS_SWRD   0b10000000

#define MANUFACTURER_ID		0xC2

#define NO_SECTORS			4096
#define NO_BLOCK_32			 512
#define NO_BLOCKS		 	 256
#define MAX_ADDRESS		0xFFFFFF

#define MAX_WRITE_LEN		256

#ifndef bool
	#define bool unsigned short
#endif

typedef union { int32_t i; float f;} Number;

enum registry{
	LAST_SEQ,
	ALTITUDE,
	REGISTRY_SIZE
};

enum sequences{
	RAMP = 0x3F,
	ACCL = 0x1F,
	DESC = 0x0F
};

int8_t MX25L_Get_ID(uint8_t data[3]);
int8_t MX25L_Get_Status(uint8_t *status);
int8_t MX25L_Set_Write_Enable(bool enable);
int8_t MX25L_Set_Status(uint8_t status);
int8_t MX25L_Erase(uint32_t address, uint32_t length);
int8_t MX25L_Sector_Erase(uint32_t address);
int8_t MX25L_Block_Erase(uint32_t address, uint8_t is64);
int8_t MX25L_Chip_Erase(void);
int8_t MX25L_Write(uint32_t address, const uint8_t * data, uint32_t length);
int8_t MX25L_Read(uint32_t address, uint8_t * data, uint32_t length);
int8_t MX25L_Protect_Block(bool bp0, bool bp1, bool bp2, bool bp3);
uint32_t READ_REGISTRY(uint32_t key);
int8_t WRITE_REGISTRY(uint32_t key, uint32_t data);
/**
 * Writes data to every address and reads it back.
 * Adds the addresses of faulty sectors to list
 * If all sectors are fine, returns 1
 */
int8_t MX25L_Check_Sectors(uint8_t *list);

#endif /* MX25L_H_ */
